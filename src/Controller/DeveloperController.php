<?php
/**
 * Created by PhpStorm.
 * User: francoeur
 * Date: 13/02/18
 * Time: 00:03
 */

namespace App\Controller;


use App\Entity\Developer;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;

class DeveloperController
{
    /**
     * @Get(
     *     path = "/developers",
     *     name = "app_list_developers",
     * )
     * @View
     */
    public function showAction(Developer $developer)
    {

        return $developer;

    }
}